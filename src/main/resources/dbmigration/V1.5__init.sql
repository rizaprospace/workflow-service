-- apply changes
create table ps_reporters (
  id                            bigint generated by default as identity not null,
  name                          varchar(255),
  email                         varchar(255),
  ss_user_id                    bigint,
  created_at                    timestamptz not null,
  modified_at                   timestamptz not null,
  created_by                    varchar(255) not null,
  modified_by                   varchar(255) not null,
  constraint pk_ps_reporters primary key (id)
);

alter table ps_issues add column reporter_id bigint;
alter table ps_issues add column status varchar(11);
alter table ps_issues add constraint ck_ps_issues_status check ( status in ('OPEN','ON_PROGRESS','CLOSED','REMEDIED'));

create index ix_ps_issues_reporter_id on ps_issues (reporter_id);
alter table ps_issues add constraint fk_ps_issues_reporter_id foreign key (reporter_id) references ps_reporters (id) on delete set null on update restrict;

