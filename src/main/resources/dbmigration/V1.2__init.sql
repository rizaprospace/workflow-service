-- apply changes
create table ps_issues_photos (
  ps_issues_id                  bigint not null,
  value                         varchar(255) not null
);

create index ix_ps_issues_photos_ps_issues_id on ps_issues_photos (ps_issues_id);
alter table ps_issues_photos add constraint fk_ps_issues_photos_ps_issues_id foreign key (ps_issues_id) references ps_issues (id) on delete restrict on update restrict;

