-- apply changes
alter table if exists ps_issues drop constraint if exists fk_ps_issues_category_id;
alter table ps_issues add constraint fk_ps_issues_category_id foreign key (category_id) references ps_categories (id) on delete set null on update restrict;
alter table ps_sites add column name varchar(255);
alter table ps_sites add column ss_site_id bigint;

