package io.prospace.ss.workflowservice.service;

import io.prospace.ss.workflowservice.domain.model.Category;
import io.prospace.ss.workflowservice.domain.model.Issue;
import io.prospace.ss.workflowservice.domain.model.Reporter;
import io.prospace.ss.workflowservice.domain.model.Site;
import io.prospace.ss.workflowservice.ports.in.IssueUseCase;
import io.prospace.ss.workflowservice.ports.in.SubmitIssueCmd;
import io.prospace.ss.workflowservice.ports.out.WorkflowRepository;

import java.util.UUID;

public final class WorkflowService implements IssueUseCase {

    private final WorkflowRepository workflowRepository;

    public WorkflowService(WorkflowRepository workflowRepository) {
        this.workflowRepository = workflowRepository;
    }

    @Override
    public Issue submitIssue(SubmitIssueCmd cmd) {

        Reporter reporter = workflowRepository.getReporterBySsId(cmd.getSsUserId());
        Category category = workflowRepository.getCategory(cmd.getCategoryId());
        Site site = workflowRepository.getSiteBySSid(cmd.getSsSiteId());

        Issue build = Issue.newReporterSubmission()
                .site(site)
                .category(category)
                .description(cmd.getDescription())
                .reportedBy(reporter)
                .build();

        return workflowRepository.saveIssue(build);
    }
}
