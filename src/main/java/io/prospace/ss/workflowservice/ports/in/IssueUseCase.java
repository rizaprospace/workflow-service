package io.prospace.ss.workflowservice.ports.in;

import io.prospace.ss.workflowservice.domain.model.Issue;

public interface IssueUseCase {
    Issue submitIssue(SubmitIssueCmd cmd);
}
