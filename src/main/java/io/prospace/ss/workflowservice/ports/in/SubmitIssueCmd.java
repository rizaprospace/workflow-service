package io.prospace.ss.workflowservice.ports.in;

import lombok.Getter;

import java.util.List;

@Getter
public class SubmitIssueCmd {
    private final Long ssUserId;
    private final Long ssSiteId;
    private final Long categoryId;
    private final String description;
    private final List<String> photos;

    public SubmitIssueCmd(Long ssUserId, Long ssSiteId, Long categoryId, String description, List<String> photos) {
        this.ssUserId = ssUserId;
        this.ssSiteId = ssSiteId;
        this.categoryId = categoryId;
        this.description = description;
        this.photos = photos;
    }
}
