package io.prospace.ss.workflowservice.ports.out;

import io.prospace.ss.workflowservice.domain.model.Category;
import io.prospace.ss.workflowservice.domain.model.Issue;
import io.prospace.ss.workflowservice.domain.model.Reporter;
import io.prospace.ss.workflowservice.domain.model.Site;

public interface WorkflowRepository {

    Reporter getReporter(Long reporterId);

    Issue saveIssue(Issue build);

    Category getCategory(Long categoryId);

    Reporter getReporterBySsId(Long ssUserId);

    Site getSiteBySSid(Long ssSiteId);
}
