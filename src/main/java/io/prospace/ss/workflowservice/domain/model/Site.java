package io.prospace.ss.workflowservice.domain.model;

import lombok.Getter;
import lombok.ToString;
import lombok.With;

@ToString
@With
@Getter
public class Site {
    private final Long id;
    private final String name;
    private final Long ssSiteId;

    public Site(Long id, String name, Long ssSiteId) {
        this.id = id;
        this.name = name;
        this.ssSiteId = ssSiteId;
    }
}
