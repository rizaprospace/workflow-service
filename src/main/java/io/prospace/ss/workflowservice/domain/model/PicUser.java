package io.prospace.ss.workflowservice.domain.model;

import lombok.Getter;

@Getter
public class PicUser {
    private final Long id;
    private final Long siteId;
    private final String name;
    private final String email;

    public PicUser(Long id, Long siteId, String name, String email) {
        this.id = id;
        this.siteId = siteId;
        this.name = name;
        this.email = email;
    }
}
