package io.prospace.ss.workflowservice.domain.model;

import lombok.Getter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
class TaskOrder {

    private final UUID orderId;
    private final OrderDetail orderDetail;
    private final String note;
    private final PicUser assignedTo;
    private final WorkflowAdmin assignedBy;
    private final LocalDateTime assignedAt;
    private final TaskStatus taskStatus;

    TaskOrder(OrderDetail orderDetail, String note, PicUser assignedTo, WorkflowAdmin assignedBy, LocalDateTime assignedAt) {
        this.orderId = UUID.randomUUID();
        this.orderDetail = orderDetail;
        this.note = note;
        this.assignedTo = assignedTo;
        this.assignedBy = assignedBy;
        this.assignedAt = assignedAt;
        this.taskStatus = TaskStatus.PENDING;
    }
}
