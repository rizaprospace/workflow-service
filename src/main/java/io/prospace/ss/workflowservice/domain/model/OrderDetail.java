package io.prospace.ss.workflowservice.domain.model;

import java.util.List;
import java.util.UUID;

public class OrderDetail {
    private final UUID requestId;
    private final String category;
    private final String description;
    private final List<String> photos;

    public OrderDetail(UUID requestId, String category, String description, List<String> photos) {
        this.requestId = requestId;
        this.category = category;
        this.description = description;
        this.photos = photos;
    }

    public static OrderDetail of(Issue issue) {
        return new OrderDetail(issue.getRequestId(), issue.getCategory().getName(), issue.getDescription(), issue.getPhotos());
    }
}
