package io.prospace.ss.workflowservice.domain.model;

public class IssueException extends RuntimeException {
    String message;
    public IssueException(String s) {
        super(s);
        this.message = s;
    }
}
