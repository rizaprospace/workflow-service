package io.prospace.ss.workflowservice.domain.model;

public enum TaskStatus {
    PENDING, DRAFT, SUBMITTED, REJECTED, APPROVED
}
