package io.prospace.ss.workflowservice.domain.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@ToString
@RequiredArgsConstructor(staticName = "of")
@Getter
@With
public class Issue {
    private final Long id;
    private final UUID requestId;
    private final Site site;
    private final Category category;
    private final String description;
    private final List<String> photos;
    private final LocalDateTime reportedAt;
    private final Reporter reportedBy;
    private final TaskInfo activeTask;
    private final Status status;


//    @Builder(builderMethodName = "fromDb")
//    public Issue(Long id, UUID requestId, Long siteId, Category category, String description, List<String> photos, LocalDateTime reportedAt, Reporter reportedBy, TaskInfo activeTask) {
//        this.id = id;
//        this.requestId = requestId;
//        this.siteId = siteId;
//        this.category = category;
//        this.description = description;
//        this.photos = photos;
//        this.reportedAt = reportedAt;
//        this.reportedBy = reportedBy;
//        this.activeTask = activeTask;
//        generateStatus();
//    }


    @Builder(builderMethodName = "newReporterSubmission")
    public Issue(Site site, Category category, String description, Reporter reportedBy) {
        this.site = site;
        this.category = category;
        this.description = description;
        this.reportedBy = reportedBy;
        this.requestId = UUID.randomUUID();
        this.reportedAt = LocalDateTime.now();
        this.status = Status.OPEN;
        this.id = null;
        this.activeTask = null;
        this.photos = null;
    }

    private Status generateStatus() {
        if (this.activeTask != null) {
            if (this.activeTask.getTaskStatus().equals(TaskStatus.PENDING)) {
                return Status.ON_PROGRESS;
            }
            if (this.activeTask.getTaskStatus().equals(TaskStatus.APPROVED)) {
                return Status.CLOSED;
            }
            if (this.activeTask.getTaskStatus().equals(TaskStatus.REJECTED)) {
                return Status.REMEDIED;
            }
        }
        return Status.OPEN;
    }

    public TaskOrder createTaskOrder(WorkflowAdmin admin, PicUser targetPic, String adminNote) {
        if (!this.status.equals(Status.OPEN)) {
            throw new IssueException("there is active task for this issue");
        }

        if (!targetPic.getSiteId().equals(this.getSite().getId())) {
            throw new IssueException("target pic has no access to this issue");
        }

        return new TaskOrder(OrderDetail.of(this), adminNote, targetPic, admin, LocalDateTime.now());
    }

}
