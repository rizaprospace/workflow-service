package io.prospace.ss.workflowservice.domain.model;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class Reporter {
    private final Long id;
    private final Long ssUserId;
    private final String name;
    private final String email;

    public Reporter(Long id, Long ssUserId, String name, String email) {
        this.id = id;
        this.ssUserId = ssUserId;
        this.name = name;
        this.email = email;
    }
}
