package io.prospace.ss.workflowservice.domain.model;

public enum Status {
    OPEN, ON_PROGRESS, CLOSED, REMEDIED
}
