package io.prospace.ss.workflowservice.domain.model;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class TaskInfo {
    private final Long taskId;
    private final TaskStatus taskStatus;
    private final LocalDateTime assignedAt;

    public TaskInfo(Long taskId, TaskStatus taskStatus, LocalDateTime assignedAt) {
        this.taskId = taskId;
        this.taskStatus = taskStatus;
        this.assignedAt = assignedAt;
    }
}
