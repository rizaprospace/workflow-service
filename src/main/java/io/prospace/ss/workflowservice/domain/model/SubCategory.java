package io.prospace.ss.workflowservice.domain.model;

public class SubCategory {
    private final String name;

    public SubCategory(String name) {
        this.name = name;
    }
}
