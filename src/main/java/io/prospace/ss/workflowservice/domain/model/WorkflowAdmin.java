package io.prospace.ss.workflowservice.domain.model;

public class WorkflowAdmin {
    private final Long id;
    private final String name;
    private final String email;

    public WorkflowAdmin(Long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }
}
