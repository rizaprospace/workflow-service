package io.prospace.ss.workflowservice.domain.model;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class Category {
    private final Long id;
    private final String name;
    private final SubCategory subCategory;

    public Category(Long id, String name, SubCategory subCategory) {
        this.id = id;
        this.name = name;
        this.subCategory = subCategory;
    }

}
