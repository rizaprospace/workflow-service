package io.prospace.ss.workflowservice.adapters.db.model;

import io.ebean.annotation.ConstraintMode;
import io.ebean.annotation.DbArray;
import io.ebean.annotation.DbEnumValue;
import io.ebean.annotation.DbForeignKey;
import io.prospace.ss.workflowservice.domain.model.Issue;
import io.prospace.ss.workflowservice.domain.model.Status;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Table(name = "ps_issues")
@Entity
@Data
public class IssueDbModel extends BaseModel {

    private UUID requestId;

    @ManyToOne
    @DbForeignKey(onDelete = ConstraintMode.SET_NULL)
    private CategoryDbModel category;

    private String description;

    @ElementCollection
    private List<String> photos;

    @ManyToOne
    @DbForeignKey(onDelete = ConstraintMode.CASCADE)
    private SiteDbModel site;

    @ManyToOne
    @DbForeignKey(onDelete = ConstraintMode.SET_NULL)
    private ReporterDbModel reporter;

    private LocalDateTime reportedAt;

    @Enumerated(EnumType.STRING)
    private IssueStatus status;

    public static IssueDbModel fromDomain(Issue build) {
        IssueDbModel issueDbModel = new IssueDbModel();
        issueDbModel.setRequestId(build.getRequestId());
        issueDbModel.setDescription(build.getDescription());
        issueDbModel.setPhotos(build.getPhotos());
        issueDbModel.setReportedAt(build.getReportedAt());
        issueDbModel.setStatus(IssueStatus.valueOf(build.getStatus().name()));
        return issueDbModel;
    }

    public static Issue toDomain(IssueDbModel model) {
        return Issue.of(
                model.getId(),
                model.getRequestId(),
                SiteDbModel.toDomain(model.getSite()),
                CategoryDbModel.toDomain(model.getCategory()),
                model.getDescription(),
                model.getPhotos(),
                model.getReportedAt(),
                ReporterDbModel.toDomain(model.getReporter()),
                null,
                Status.valueOf(model.getStatus().name())
        );
    }


//    private Reporter reportedBy;
//    private TaskInfo activeTask;
//    private Status status;
}
