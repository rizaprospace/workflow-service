package io.prospace.ss.workflowservice.adapters.db.model;

public enum IssueStatus {
    OPEN, ON_PROGRESS, CLOSED, REMEDIED
}
