package io.prospace.ss.workflowservice.adapters.db.model;

import io.ebean.Model;
import io.ebean.annotation.WhenCreated;
import io.ebean.annotation.WhenModified;
import io.ebean.annotation.WhoCreated;
import io.ebean.annotation.WhoModified;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * @author Riza Maizarani on 02/08/2018
 * @project conference_management_system_backend
 */
@Data
@MappedSuperclass
public class BaseModel extends Model {

    @Id
    @GeneratedValue
    Long id;

    @WhenCreated
    private LocalDateTime createdAt;
    @WhenModified
    private LocalDateTime modifiedAt;
    @WhoCreated
    private String createdBy;
    @WhoModified
    private String modifiedBy;

}
