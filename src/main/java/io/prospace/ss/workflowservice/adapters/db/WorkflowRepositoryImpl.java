package io.prospace.ss.workflowservice.adapters.db;

import io.ebean.Database;
import io.prospace.ss.workflowservice.adapters.db.model.CategoryDbModel;
import io.prospace.ss.workflowservice.adapters.db.model.IssueDbModel;
import io.prospace.ss.workflowservice.adapters.db.model.ReporterDbModel;
import io.prospace.ss.workflowservice.adapters.db.model.SiteDbModel;
import io.prospace.ss.workflowservice.domain.model.Category;
import io.prospace.ss.workflowservice.domain.model.Issue;
import io.prospace.ss.workflowservice.domain.model.Reporter;
import io.prospace.ss.workflowservice.domain.model.Site;
import io.prospace.ss.workflowservice.ports.out.WorkflowRepository;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class WorkflowRepositoryImpl implements WorkflowRepository {

    private final Database database;

    public WorkflowRepositoryImpl(Database database) {
        this.database = database;
    }

    @Override
    public Reporter getReporter(Long reporterId) {
        return null;
    }

    @Override
    public Issue saveIssue(Issue build) {
        IssueDbModel issueDbModel = IssueDbModel.fromDomain(build);
        issueDbModel.setCategory(database.find(CategoryDbModel.class, build.getCategory().getId()));
        issueDbModel.setSite(database.find(SiteDbModel.class, build.getSite().getId()));
        issueDbModel.setReporter(database.find(ReporterDbModel.class, build.getReportedBy().getId()));
        database.save(issueDbModel);
        return IssueDbModel.toDomain(issueDbModel);
    }

    @Override
    public Category getCategory(Long categoryId) {
        CategoryDbModel model = database.find(CategoryDbModel.class).where().idEq(categoryId).findOneOrEmpty().orElseThrow(() -> new EntityNotFoundException("category not found"));
        return new Category(model.getId(), model.getName(), null);
    }

    @Override
    public Reporter getReporterBySsId(Long ssUserId) {
        ReporterDbModel reporterDbModel = database.find(ReporterDbModel.class).where().eq("ssUserId", ssUserId).findOneOrEmpty()
                .orElseThrow(() -> new EntityNotFoundException("user not found"));

        return ReporterDbModel.toDomain(reporterDbModel);
    }

    @Override
    public Site getSiteBySSid(Long ssSiteId) {
        SiteDbModel siteDbModel = database.find(SiteDbModel.class).where().eq("ssSiteId", ssSiteId).findOneOrEmpty()
                .orElseThrow(() -> new EntityNotFoundException("site not found"));

        return SiteDbModel.toDomain(siteDbModel);
    }
}
