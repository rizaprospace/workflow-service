package io.prospace.ss.workflowservice.adapters.db.model;

import io.ebean.annotation.ConstraintMode;
import io.ebean.annotation.DbForeignKey;
import io.prospace.ss.workflowservice.domain.model.Site;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Table(name = "ps_sites")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SiteDbModel extends BaseModel {

    private String name;
    private Long ssSiteId;

    @OneToMany(mappedBy = "site", cascade = CascadeType.REMOVE)
    private List<IssueDbModel> issues;

    public static Site toDomain(SiteDbModel siteDbModel) {
        return new Site(siteDbModel.getId(), siteDbModel.getName(), siteDbModel.getSsSiteId());
    }
}
