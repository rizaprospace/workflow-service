package io.prospace.ss.workflowservice.adapters.db.model;

import io.prospace.ss.workflowservice.domain.model.Category;
import io.prospace.ss.workflowservice.domain.model.SubCategory;
import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import java.util.List;

@Table(name = "ps_categories")
@Entity
@Data
public class CategoryDbModel extends BaseModel {

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "category")
    private List<IssueDbModel> issues;

    public static Category toDomain(CategoryDbModel model) {
        return new Category(model.getId(), model.getName(), null);
    }
}
