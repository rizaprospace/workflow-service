package io.prospace.ss.workflowservice.adapters.db.model;


import io.prospace.ss.workflowservice.domain.model.Reporter;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Table(name = "ps_reporters")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReporterDbModel extends BaseModel {

    private String name;
    private String email;
    private Long ssUserId;

    @OneToMany(mappedBy = "reporter")
    private List<IssueDbModel> issues;

    public static Reporter toDomain(ReporterDbModel reporter) {
        return new Reporter(reporter.getId(), reporter.getSsUserId(), reporter.getName(), reporter.getEmail());
    }
}
