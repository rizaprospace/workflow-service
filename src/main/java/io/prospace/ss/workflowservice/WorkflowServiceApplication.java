package io.prospace.ss.workflowservice;

import io.ebean.Database;
import io.prospace.ss.workflowservice.domain.model.Issue;
import io.prospace.ss.workflowservice.ports.in.SubmitIssueCmd;
import io.prospace.ss.workflowservice.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class WorkflowServiceApplication {

    @Autowired
    WorkflowService workflowService;

    @Autowired
    Database database;

    public static void main(String[] args) {
        SpringApplication.run(WorkflowServiceApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void starting() {

//        SiteDbModel s = new SiteDbModel("site-1", 5L, null);
//        s.save();
//        ReporterDbModel r = new ReporterDbModel("riza", "riza@prospace.io", 2L, null);
//        r.save();

        Issue asdasd = workflowService.submitIssue(new SubmitIssueCmd(2L, 5L, 1L, "asdasd", null));

        System.out.println(asdasd.toString());
    }

}
