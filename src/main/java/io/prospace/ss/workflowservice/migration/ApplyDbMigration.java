package io.prospace.ss.workflowservice.migration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.ebean.DatabaseFactory;
import io.ebean.config.DatabaseConfig;
import io.ebean.migration.MigrationConfig;
import io.ebean.migration.MigrationRunner;

import javax.sql.DataSource;

public class ApplyDbMigration {

    public static void main(String[] args) {

        String path = "src/main/resources";

        System.setProperty("disableTestProperties", "true");

        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.postgresql.Driver");

        config.setJdbcUrl("jdbc:postgresql://localhost:5432/workflow-db");
        config.setUsername("postgres");
        config.setPassword("123456");

        DataSource dataSource = new HikariDataSource(config);
        MigrationConfig migrationConfig = new MigrationConfig();
        MigrationRunner runner = new MigrationRunner(migrationConfig);
        runner.run(dataSource);


//        Ebean.getServer("db");

        System.out.println("done");
    }
}
