package io.prospace.ss.workflowservice.migration;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.ebean.annotation.Platform;
import io.ebean.config.DatabaseConfig;
import io.ebean.dbmigration.DbMigration;

import java.io.IOException;

public class DBMigrationGenerator {

    public static void main(String[] args) throws IOException {

        String path = "src/main/resources";

//        System.out.println(path);

//        System.setProperty("ddl.migration.version", "1.39");

        System.setProperty("ddl.migration.name", "init");

//        System.setProperty("ddl.migration.pendingDropsFor", "1.23");

//        ObjectMapper objectMapper = new ObjectMapper();
//

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/workflow-db");
        config.setUsername("postgres");
        config.setPassword("123456");
        config.setDriverClassName("org.postgresql.Driver");

        DatabaseConfig databaseConfig = new DatabaseConfig();
        databaseConfig.setDataSource(new HikariDataSource(config));

        DbMigration dbMigration = DbMigration.create();

        dbMigration.setPlatform(Platform.POSTGRES);
        dbMigration.setStrictMode(false);
        dbMigration.setPathToResources(path);
        dbMigration.setApplyPrefix("V");
        dbMigration.setServerConfig(databaseConfig);

        dbMigration.generateMigration();

        System.out.println("done");

    }
}
