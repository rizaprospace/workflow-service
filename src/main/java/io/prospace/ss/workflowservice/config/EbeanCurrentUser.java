package io.prospace.ss.workflowservice.config;

import io.ebean.config.CurrentUserProvider;
import org.springframework.stereotype.Component;

@Component
class EbeanCurrentUser implements CurrentUserProvider {

    @Override
    public Object currentUser() {
        return "workflow-service";
    }
}