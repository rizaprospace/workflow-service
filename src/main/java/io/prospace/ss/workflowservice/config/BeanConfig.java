package io.prospace.ss.workflowservice.config;

import io.prospace.ss.workflowservice.ports.out.WorkflowRepository;
import io.prospace.ss.workflowservice.service.WorkflowService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    WorkflowService workflowService(WorkflowRepository workflowRepository){
        return new WorkflowService(workflowRepository);
    }
}
